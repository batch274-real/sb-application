package com.zuitt.example.Application.models;

import java.io.Serializable;

public class JwtRequest implements Serializable {
    private  static  final long serialVersionID = 5926468583005150707L;
    private String username;
    private String password;
    public JwtRequest(){}
    public JwtRequest(String username, String Password){
     this.setUsername(username);
     this.setPassword(password);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
