package com.zuitt.example.Application.exceptions;

public class UserException extends Exception {
    public UserException(String message){
        super(message);
    }
}
